# This file is part of django-ca (https://github.com/mathiasertl/django-ca).
#
# django-ca is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# django-ca is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License along with django-ca. If not, see
# <http://www.gnu.org/licenses/>.

"""Shared typehints for tests."""

import typing
from typing import Any, Dict

from django_ca.models import DjangoCAModel

DjangoCAModelTypeVar = typing.TypeVar("DjangoCAModelTypeVar", bound=DjangoCAModel)


CertFixtureData = Dict[str, Any]


class _OcspFixtureData(typing.TypedDict):
    name: str
    filename: str


class OcspFixtureData(_OcspFixtureData, total=False):
    """Fixture data for OCSP requests.

    Keys:

    * name (str): name of the fixture
    * filename (str): name of the file of the stored request
    * nonce (str, optional): Nonce used in the request
    """

    nonce: str


class FixtureData(typing.TypedDict):
    """Fixture data loaded/stored from JSON."""

    certs: Dict[str, CertFixtureData]
