###################################
``django_ca.constants`` - constants
###################################

.. automapping:: django_ca.constants.ELLIPTIC_CURVE_TYPES
   :annotation:

.. automapping:: django_ca.constants.EXTENDED_KEY_USAGE_NAMES
   :no-value:

   test

.. automapping:: django_ca.constants.EXTENSION_DEFAULT_CRITICAL
   :no-value:

.. automapping:: django_ca.constants.EXTENSION_KEYS
   :no-value:

.. autodata:: django_ca.constants.EXTENSION_KEY_OIDS
   :no-value:

.. automapping:: django_ca.constants.HASH_ALGORITHM_NAMES
   :no-value:

.. automapping:: django_ca.constants.KEY_USAGE_NAMES
   :no-value:

.. automapping:: django_ca.constants.NAME_OID_NAMES
   :no-value:

.. automapping:: django_ca.constants.NAME_OID_TYPES
   :no-value:

.. automodule:: django_ca.constants
   :members: ReasonFlags

