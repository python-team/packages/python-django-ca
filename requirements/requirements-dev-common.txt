# modules imported in devscripts/
GitPython==3.1.31
Jinja2==3.1.2
PyYAML==6.0
# sphinx_rtd_theme==1.2.2 requires Sphinx<7:
#   https://github.com/readthedocs/sphinx_rtd_theme/issues/1463
#   https://github.com/readthedocs/sphinx_rtd_theme/issues/1323
Sphinx<7
coverage[toml]==7.2.7
semantic-version==2.10.0
tabulate==0.9.0
termcolor==2.3.0
tomli==2.0.1; python_version < '3.11'
